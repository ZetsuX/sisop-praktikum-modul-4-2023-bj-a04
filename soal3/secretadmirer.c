#define FUSE_USE_VERSION 28
#define BUFFER_SIZE 1024

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

static const char *dirpath = "/home/kev/Kuliah/Sisop/Praktikum4/files/inifolderetc/sisop";
static const char *setPW = "terserah";

void base64Encode(const char *filePath) {
    FILE *file, *tempFile;
    BIO *bio, *b64;
    char buffer[BUFFER_SIZE];
    size_t bytesRead;

    file = fopen(filePath, "rb");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return;
    }

    tempFile = tmpfile();
    if (tempFile == NULL) {
        printf("Failed to create a temporary file.\n");
        fclose(file);
        return;
    }

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_fp(tempFile, BIO_NOCLOSE);
    bio = BIO_push(b64, bio);

    while ((bytesRead = fread(buffer, 1, BUFFER_SIZE, file)) > 0) {
        BIO_write(bio, buffer, bytesRead);
    }

    BIO_flush(bio);

    rewind(tempFile);

    fclose(file);
    file = fopen(filePath, "wb");
    if (file == NULL) {
        printf("Failed to open the file for writing.\n");
        fclose(tempFile);
        return;
    }

    while ((bytesRead = fread(buffer, 1, BUFFER_SIZE, tempFile)) > 0) {
        fwrite(buffer, 1, bytesRead, file);
    }

    BIO_free_all(bio);
    fclose(file);
    fclose(tempFile);
    return;
}

bool needEncode(const char *string) {
    if (string == NULL) return false;
    return (strstr(string, "/L") != NULL || strstr(string, "/01101100") != NULL || strstr(string, "/U") != NULL || strstr(string, "/01110101") != NULL || strstr(string, "/T") != NULL || strstr(string, "/01110100") != NULL || strstr(string, "/H") != NULL || strstr(string, "/01101000") != NULL);
}

void toUpper(char *str) {
    while (*str) {
        *str = toupper((unsigned char)*str);
        str++;
    }
}

void toLower(char *str) {
    while (*str) {
        *str = tolower((unsigned char)*str);
        str++;
    }
}

char* asciiConv(const char* name) {
    int length = strlen(name);
    char* result = (char*)malloc((9 * length + 1) * sizeof(char));
    if (result == NULL) {
        fprintf(stderr, "Memory allocation failed\n");
        return NULL;
    }

    int index = 0;
    for (int i = 0; i < length; i++) {
        unsigned char c = name[i];
        for (int j = 7; j >= 0; j--) {
            int bit = (c >> j) & 1;
            index += sprintf(result + index, "%d", bit);
        }
        result[index++] = ' ';
    }
    result[index - 1] = '\0';

    return result;
}

void changeName(char *path, const char *newName) {
    char newPath[1000];
    strncpy(newPath, path, sizeof(newPath));
    newPath[sizeof(newPath) - 1] = '\0';

    char *lastSlash = strrchr(newPath, '/');
    if (lastSlash != NULL) {
        strcpy(lastSlash + 1, newName);
        rename(path, newPath);

        strncpy(path, newPath, strlen(newPath) + 1);
    }
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;

        struct stat st;
        char cpath[2000];

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        sprintf(cpath, "%s/%s", fpath, de->d_name);

        res = (filler(buf, de->d_name, &st, 0));
        if (res!=0) break;

        if (S_ISDIR(st.st_mode)) {
            char dName[1000];
            strcpy(dName, de->d_name);

            if (strlen(dName) <= 4) {
                strcpy(dName, asciiConv(dName));
            } else {
                toUpper(dName);
            }
            changeName(cpath, dName);

            // printf("%s : %s\n", de->d_name, cpath);
            xmp_readdir(cpath, buf, filler, offset, fi);
        } else {
            char fName[1000];
            strcpy(fName, de->d_name);

            if (strlen(fName) <= 4) {
                strcpy(fName, asciiConv(fName));
            } else {
                toLower(fName);
            }
            changeName(cpath, fName);

            if (needEncode(cpath)) base64Encode(cpath);

            // printf("%s : %s\n", de->d_name, cpath);
        }
    }

    closedir(dp);
    return 0;
}


static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void)fi;

    if (strcmp(path, "/") == 0) {
        path = dirpath;
    } else {
        char fpath[1000];
        sprintf(fpath, "%s%s", dirpath, path);
        if (access(fpath, F_OK) == 0) {
            char inPW[100];
            printf("Enter your Password: ");
            fflush(stdout);
            fgets(inPW, sizeof(inPW), stdin);

            int len = strlen(inPW);
            if (len > 0) inPW[len - 1] = '\0';

            while (strcmp(inPW, setPW) != 0) {
                printf("The password you just entered is incorrect! Please try again.\n\nEnter your Password: ");
                fflush(stdout);
                fgets(inPW, sizeof(inPW), stdin);

                len = strlen(inPW);
                if (len > 0) inPW[len - 1] = '\0';
            }
        }
    }

    return 0;
}

int xmp_rename(const char *old, const char *new)
{
    char oldPath[1000], newPath[1000];
    sprintf(oldPath, "%s%s", dirpath, old);
    sprintf(newPath, "%s%s", dirpath, new);

    int res = rename(oldPath, newPath);
    if (res == -1)
        return -errno;

    return 0;
}

int xmp_mkdir(const char *path, mode_t mode)
{
    char dirPath[1000];
    sprintf(dirPath, "%s%s", dirpath, path);

    int res = mkdir(dirPath, mode);
    if (res == -1)
        return -errno;

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .open = xmp_open,
    .rename = xmp_rename,
    .mkdir = xmp_mkdir,
};

int main(int  argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}