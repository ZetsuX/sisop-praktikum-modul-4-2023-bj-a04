#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <libgen.h>

FILE *log_file;

// Save log file when the program is terminated
void closeLogFile() {
    if (log_file != NULL) {
        fclose(log_file);
        log_file = NULL;
    }
}

// Write log file
void write_log(const char *status, const char *command, const char *description) {
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", tm);

    FILE *log_file = fopen("logmucatatsini.txt", "a");
    if (log_file) {
        fprintf(log_file, "%s::%s::%s::%s-%s\n", status, timestamp, command, getlogin(), description);
        fclose(log_file);
    }
}

// Get file attribute
static int xmp_getattr(const char *path, struct stat *stbuf) {  
    int res;

    res = lstat(path, stbuf);

    if (res == -1)
        return -errno;
    
    return 0;
}

// Read directory
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(path);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (filler(buf, de->d_name, &st, 0))
            break;
    }

    closedir(dp);

    return 0;
}

// Read data from an open file
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    int fd;
    int res;
    (void)fi;

    fd = open(path, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

// Make a directory
static int xmp_mkdir(const char *path, mode_t mode){
    // printf("CREATE %s\n", path);
    char desc[1000];
    sprintf(desc, "Create directory %s.", path);

    if (strstr(path, "restricted")) {
        write_log("FAILED", "MKDIR", desc);
        return -EPERM;
    }

    int res;

    res = mkdir(path, mode);
    if (res == -1){
        write_log("FAILED", "MKDIR", strerror(errno));
        return -errno;
    }

    write_log("SUCCESS", "MKDIR", desc);

    return 0;
}

// Rename a file or a directory
static int xmp_rename(const char *from, const char *to) {
    // printf("RENAME From %s to %s\n", from, to);
    char desc[1000];
    sprintf(desc, "Rename from %s to %s.", from, to);

    char bypass[1000];
    sprintf(bypass, "%s", strstr(from, "bypass"));

    if (((strstr(from, "restricted") && !strstr(from, "bypass") && (!strstr(to, "bypass"))) || (strstr (bypass, "restricted"))  )) {
        write_log("FAILED", "RENAME", desc);
        return -EPERM;
    }

    int res = rename(from, to);
    if (res == -1)
        return -errno;

    write_log("SUCCESS", "RENAME", desc);

    return 0;
}

// Remove a file
static int xmp_unlink(const char *path) {
    // printf("REMOVE file %s\n", path);
    char desc[1000];
    sprintf(desc, "Remove file %s.", path);

    if (strstr(path, "restricted") && !strstr(path, "bypass")) {
        write_log("FAILED", "UNLINK", desc);
        return -EPERM;
    }

    int res = unlink(path);
    if (res == -1)
        return -errno;

    write_log("SUCCESS", "UNLINK", desc);

    return 0;
}

// Remove a directory
static int xmp_rmdir(const char* path){
    // printf("REMOVE directory %s\n", path);
    char desc[1000];
    sprintf(desc, "Remove directory %s.", path);

    if (strstr(path, "restricted") && !strstr(path, "bypass")) {
        write_log("FAILED", "RMDIR", desc);
        return -EPERM;
    }

    int res = rmdir(path);
    if (res == -1)
        return -errno;

    write_log("SUCCESS", "RMDIR", desc);

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
};

int main(int argc, char *argv[]) {

    log_file = fopen("logmucatatsini.txt", "w");
    atexit(closeLogFile);

    umask(0);
    return fuse_main(argc, argv, &xmp_oper, "nanaxgerma/src_data/");
}
