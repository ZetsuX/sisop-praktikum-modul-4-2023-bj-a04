#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

void main(){
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");
    system("rm fifa-player-stats-database.zip");
    system("awk -F ',' '$3 < 25 && $8 > 85 && $9 != \"Manchester City\" {"
            "print \"=========================================================\";""print \"Name: \" $2;""print \"Age: \" $3;""print \"Club: \" $9;""print \"Nationality: \" $5;""print \"Potential: \" $8;""print \"Wage: \" $12;""print \"Photo: \" $4;"
            "}' FIFA23_official_data.csv");
    
}