# Praktikum Sistem Operasi 2003 Modul 4 (Kelompok A04)

## Daftar Isi

-   [Anggota Kelompok A04](#anggota-kelompok)
-   [Permasalahan 1](#permasalahan-1)
-   [Permasalahan 2](#permasalahan-2)
-   [Permasalahan 3](#permasalahan-3)

## Anggota Kelompok

-   Widian Sasi Disertasiani / 5025211024
-   Kevin Nathanael Halim / 5025211140
-   Andhika Lingga Mariano / 5025211161

## Permasalahan 1

## Latar Belakang Permasalahan
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

### Catatan
- Cantumkan file hubdocker.txt yang berisi URL Docker Hub kalian (public).
- Perhatikan port  pada masing-masing instance.

### Permasalahan
- Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 
- Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.
- Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
- Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.
Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.
- Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

### Penjelasan code

- storage.c
```c
void main(){
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");
    system("rm fifa-player-stats-database.zip");
    system("awk -F ',' '$3 < 25 && $8 > 85 && $9 != \"Manchester City\" {"
            "print \"=========================================================\";""print \"Name: \" $2;""print \"Age: \" $3;""print \"Club: \" $9;""print \"Nationality: \" $5;""print \"Potential: \" $8;""print \"Wage: \" $12;""print \"Photo: \" $4;"
            "}' FIFA23_official_data.csv");}
```
Pada file pertama yaitu storage.c, berisikan perintah untuk mendownload dataset pemain fifa, yang kemudian diunzip, lalu file zipnya dihapus, tidak lupa melakukan filter dengan ketentuan soal dan menampilkan nama, usia, club, nationality, potential, wage, serta photo. 

- Dockerfile
```
FROM ubuntu:latest
```
Mengambil source image 

```
RUN apt-get update
RUN apt install unzip -y
RUN apt install python3 -y
RUN apt install pip -y
RUN pip install kaggle
```
Mengintall aplikasi yang akan dibutuhkan nantinya

```
COPY storage.c .
```
Mengcopy strogae.c ke workplace

```
ENV KAGGLE_USERNAME="widiansasi"
ENV KAGGLE_KEY="796aae233b28f96ddd19f1a8a8f8c730"
```
Setting environtment melalui file Json kaggle

```
RUN gcc -o storage storage.c
```
Mengcompile file storage,c

```
CMD ["./storage"]
```
Agar kektika build docker dirun file c akan langsung dirun juga.

### Output

<img src="https://media.discordapp.net/attachments/1083783734534357083/1114488182289993779/image.png?width=1002&height=453">

<img src="https://media.discordapp.net/attachments/1083783734534357083/1114488404407754905/image.png?width=1002&height=469">

- Docker-compose

```
version: '3'

services:
  Barcelona:
    build: ./Barcelona
    image: widiansasi/storage-app
    deploy:
      replicas: 5

 Napoli:
    build: ./Napoli
    image: widiansasi/storage-app
    deploy:
      replicas: 5
```
membuat dua container Barrcelona dan Napoli yang berisikan  masing-masing 5 instansi, dengan sumber image dari widiansasi/storage-app, dan lokasi folder dockerfile pada Barccelona dan Napoli.

### Output

<img src="https://media.discordapp.net/attachments/1083783734534357083/1114495010444951592/853cef57-1646-433e-a0cc-f918d0f9c367.png?width=1119&height=629">


## Permasalahan 2

### Latar Belakang
Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di <a href="https://www.germa.dev/">germa.dev</a> sebagai programmer.

### Perintah Permasalahan
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file <a href="https://drive.google.com/file/d/1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i/view?usp=sharing">.zip</a> yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:
- Apabila terdapat file yang mengandung kata **restricted**, maka file tersebut tidak dapat di-rename ataupun dihapus.
- Apabila terdapat folder yang mengandung kata **restricted**, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.

**Contoh**:<br>
/jalan/keputih/perintis/iv/sangat<span style="background-color: #f4cccc">restricted</span>loh/gang/kane.txt

Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata sihir yaitu **bypass** yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya. <br>
**Case invalid untuk bypass**:<br>
/jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt<br>
**Case valid untuk bypass**:<br>
/jalan/keputih/perintis/iv/tadirestrictedtapi<span style="background-color: #d9ead3">bypass</span>kok/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/<span style="background-color: #d9ead3">bypass</span>banget/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/<span style="background-color: #d9ead3">bypass</span>banget/kaneudahtutup.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakai<span style="background-color: #d9ead3">bypass</span>.txt

Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama **germa.c**, yang mana dapat melakukan *make folder*, *rename* *file* dan *folder*, serta *delete* *file* dan *folder*. Untuk mengujinya, maka lakukan hal-hal berikut:

- Buatlah folder **productMagang** pada folder **/src_data/germa/products/restricted_list/**. Kemudian, buatlah folder **projectMagang** pada /**src_data/germa/projects/restricted_list/**. Akan tetapi, hal tersebut akan gagal.
- Ubahlah nama folder dari **restricted_list** pada folder **/src_data/germa/projects/restricted_list/** menjadi **/src_data/germa/projects/bypass_list/**. Kemudian, buat folder **projectMagang** di dalamnya.
- Karena folder **projects** menjadi dapat diakses, ubahlah folder **filePenting** di dalam folder **projects** menjadi **restrictedFilePenting** agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.
- Terakhir, coba kamu hapus semua **fileLama** di dalam folder **restrictedFileLama**. Jangan lupa untuk menambahkan kata **sihir** pada folder tersebut agar folder tersebut dapat terhapus
- Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file **logmucatatsini.txt** di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa. <br>
Adapun format logging yang harus kamu buat adalah sebagai berikut. 

    **[STATUS]::[dd]/[MM]/[yyyy]-[HH]:[mm]:[ss]::[CMD]::[DESC]**

    Dengan keterangan sebagai berikut.
    - **STATUS**: SUCCESS atau FAILED.
    - **dd**: 2 digit tanggal.
    - **MM**: 2 digit bulan.
    - **yyyy**: 4 digit tahun.
    - **HH**: 24-hour clock hour, with a leading 0 (e.g. 22).
    - **mm**: 2 digit menit.
    - **ss**: 2 digit detik.
    - **CMD**: command yang digunakan (MKDIR atau RENAME atau RMDIR atau RMFILE atau lainnya).
    - **DESC**: keterangan action, yaitu:
        - [User]-Create directory x.
        - [User]-Rename from x to y.
        - [User]-Remove directory x.
        - [User]-Remove file x.

    **Contoh**: <br>
    **SUCCESS::17/05/2023-19:31:56::RENAME::Oky-Rename from x to y**

### Catatan
- Keterangan untuk file .zip.
    - Folder src_data adalah folder source FUSE.
    - Folder dest_data adalah folder destination FUSE.

### Tujuan Permasalahan
Membuat sebuah FUSE bernama **germa.c** yang dapat membuat folder, rename file dan folder, serta delete file dan folder. 

### Konsep Penyelesaian
- Mendefinisikan fungsi untuk setiap command (membuat folder, rename file dan folder, delete file dan folder) di dalam struct FUSE. 
- Memberi batasan pada tiap command agar sesuai dengan ketentuan pada soal (restricted dan bypass).
- Menyimpan pesan log untuk setiap command yang dijalankan di dalam file *logmucatatsini.txt*.

### Implementasi dan Penjelasan (Kode)

1. Mengimport library yang diperlukan dan mendeklarasikan file log_file untuk menyimpan log.

    ```c
    #define FUSE_USE_VERSION 28
    #include <fuse.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <unistd.h>
    #include <fcntl.h>
    #include <dirent.h>
    #include <errno.h>
    #include <sys/time.h>
    #include <libgen.h>

    FILE *log_file; 
    ```

2. Membuat fungsi **closeLogFile()** untuk menutup dan menyimpan file log setelah program selesai dijalankan

    ```c
    void closeLogFile() {
        if (log_file != NULL) {
            fclose(log_file);
            log_file = NULL;
        }
    }
    ```

3. Membuat fungsi **write_log()** untuk memasukkan log ke dalam file *logmucatatsini.txt* dengan format seperti yang telah ditentukan pada soal.

    ```c
    void write_log(const char *status, const char *command, const char *description) {
        time_t t = time(NULL);
        struct tm *tm = localtime(&t);
        char timestamp[20];
        strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", tm);

        FILE *log_file = fopen("logmucatatsini.txt", "a");
        if (log_file) {
            fprintf(log_file, "%s::%s::%s::%s-%s\n", status, timestamp, command, getlogin(), description);
            fclose(log_file);
        }
    }
    ```

4. Membuat fungsi **xmp_getattr()** untuk mendapatkan atribut dari suatu file.

    ```c
    static int xmp_getattr(const char *path, struct stat *stbuf) {  
        int res;

        res = lstat(path, stbuf);

        if (res == -1)
            return -errno;
        
        return 0;
    }
    ```

5. Membuat fungsi **xmp_readdir()** untuk membaca suatu directory. 
    
    ```c
    static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
        DIR *dp;
        struct dirent *de;
        (void)offset;
        (void)fi;

        dp = opendir(path);

        if (dp == NULL)
            return -errno;

        while ((de = readdir(dp)) != NULL) {
            struct stat st;

            memset(&st, 0, sizeof(st));

            st.st_ino = de->d_ino;
            st.st_mode = de->d_type << 12;

            if (filler(buf, de->d_name, &st, 0))
                break;
        }

        closedir(dp);

        return 0;
    }
    ```

6. Membuat fungsi **xmp_read()** untuk membaca isi dari suatu file.

    ```c
    static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
        int fd;
        int res;
        (void)fi;

        fd = open(path, O_RDONLY);

        if (fd == -1)
            return -errno;

        res = pread(fd, buf, size, offset);

        if (res == -1)
            res = -errno;

        close(fd);

        return res;
    }
    ```

7. Membuat fungsi **xmp_mkdir()** untuk membuat direktori baru. Apabila terdapat kata 'restricted' dalam path direktori, maka direktori tidak dapat dibuat.

    ```c
    static int xmp_mkdir(const char *path, mode_t mode){
        char desc[1000];
        sprintf(desc, "Create directory %s.", path);

        if (strstr(path, "restricted")) {
            write_log("FAILED", "MKDIR", desc);
            return -EPERM;
        }

        int res;

        res = mkdir(path, mode);
        if (res == -1){
            write_log("FAILED", "MKDIR", strerror(errno));
            return -errno;
        }

        write_log("SUCCESS", "MKDIR", desc);

        return 0;
    }
    ```

8. Membuat fungsi **xmp_rename()** untuk mengubah nama file atau nama direktori. Apabila terdapat kata `restricted` dalam nama direktori lama dan tidak ada kata `bypass` dalam nama direktori lama maupun baru, maka rename akan gagal. Selain itu, apabila terdapat subfolder dengan nama `restricted` dalam suatu folder dengan nama `bypass`, maka rename juga akan gagal.

    ```c
    static int xmp_rename(const char *from, const char *to) {
        char desc[1000];
        sprintf(desc, "Rename from %s to %s.", from, to);

        char bypass[1000];
        sprintf(bypass, "%s", strstr(from, "bypass"));

        if (((strstr(from, "restricted") && !strstr(from, "bypass") && (!strstr(to, "bypass"))) || (strstr (bypass, "restricted"))  )) {
            write_log("FAILED", "RENAME", desc);
            return -EPERM;
        }

        int res = rename(from, to);
        if (res == -1)
            return -errno;

        write_log("SUCCESS", "RENAME", desc);

        return 0;
    }
    ```

9. Membuat fungsi **xmp_unlink()** untuk menghapus file. Apabila nama file mengandung kata `restricted` dan tidak mengandung kata `bypass`, maka file tidak dapat dihapus.

    ```c
    static int xmp_unlink(const char *path) {
        char desc[1000];
        sprintf(desc, "Remove file %s.", path);

        if (strstr(path, "restricted") && !strstr(path, "bypass")) {
            write_log("FAILED", "UNLINK", desc);
            return -EPERM;
        }

        int res = unlink(path);
        if (res == -1)
            return -errno;

        write_log("SUCCESS", "UNLINK", desc);

        return 0;
    }
    ```

10. Membuat fungsi **xmp_rmdir()** untuk menghapus direktori. Apabila nama direktori mengandung kata `restricted` dan tidak mengandung kata `bypass`, maka direktori tidak dapat dihapus.

    ```c
    static int xmp_rmdir(const char* path){
        char desc[1000];
        sprintf(desc, "Remove directory %s.", path);

        if (strstr(path, "restricted") && !strstr(path, "bypass")) {
            write_log("FAILED", "RMDIR", desc);
            return -EPERM;
        }

        int res = rmdir(path);
        if (res == -1)
            return -errno;

        write_log("SUCCESS", "RMDIR", desc);

        return 0;
    }
    ```

11. Mendefinisikan struct **xmp_oper()** yang berisi fungsi-fungsi xmp diatas.

    ```c    
    static struct fuse_operations xmp_oper = {
        .getattr = xmp_getattr,
        .readdir = xmp_readdir,
        .read = xmp_read,
        .mkdir = xmp_mkdir,
        .rename = xmp_rename,
        .unlink = xmp_unlink,
        .rmdir = xmp_rmdir,
    };
    ```

12. Terakhir, fungsi main akan membuka file *logmucatatsini.txt*, memanggil fungsi **closeLogFile()** saat program berhenti, dan memanggil fungsi **fuse_main()**.

    ```c
    int main(int argc, char *argv[]) {
        log_file = fopen("logmucatatsini.txt", "w");
        atexit(closeLogFile);

        umask(0);
        return fuse_main(argc, argv, &xmp_oper, "nanaxgerma/src_data/");
    }
    ```

### Output

- Membuat folder **productMagang** pada folder **/src_data/germa/products/restricted_list/** dan folder **projectMagang** pada folder **/src_data/germa/projects/restricted_list/**.
    
    <img src="https://cdn.discordapp.com/attachments/1102231088945963098/1114482144031555614/image.png">
    
    <img src="https://cdn.discordapp.com/attachments/1102231088945963098/1114482144258039838/image.png">

- Mengubah nama folder dari **restricted_list** pada folder **/src_data/germa/projects/restricted_list/** menjadi **/src_data/germa/projects/bypass_list/** lalu buat folder **projectMagang** di dalamnya.
    
    <img src="https://cdn.discordapp.com/attachments/1102231088945963098/1114482790499618906/image.png">
    
    <img src="https://cdn.discordapp.com/attachments/1102231088945963098/1114482377096446002/image.png">

- Mengubah folder **filePenting** di dalam folder **projects** menjadi **restrictedFilePenting** agar secure kembali. 

    <img src="https://cdn.discordapp.com/attachments/1102231088945963098/1114483111317737473/image.png">

    Mencoba mengubah nama file yang ada di dalamnya.

    <img src="https://cdn.discordapp.com/attachments/1102231088945963098/1114483111552626798/image.png">

- Menghapus semua **fileLama** di dalam folder **restrictedFileLama**. 

    <img src="https://cdn.discordapp.com/attachments/1102231088945963098/1114483655721627689/image.png">

    Menambahkan kata `bypass` pada folder tersebut agar folder tersebut dapat terhapus
    
    <img src="https://cdn.discordapp.com/attachments/1102231088945963098/1114483655990050856/image.png">
    
    <img src="https://cdn.discordapp.com/attachments/1102231088945963098/1114483656262697080/image.png">

- Isi file *logmucatatsini.txt*

    <img src="https://cdn.discordapp.com/attachments/1102231088945963098/1114485005117304863/image.png">

### Kendala yang sempat dihadapi
- Saat ingin melakukan mount ke folder dest_data, folder dest_data malah terhapus.
- Log message tidak muncul pada file logmucatatsini.txt.


## Permasalahan 3

### Latar Belakang

Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.

### Perintah Permasalahan

-   Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c.
-   Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64. Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).
-   Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.
-   Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).
-   Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut. Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi tersebut. Container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun.
-   Kalian masih penasaran siapa yang dikagumi Dhafin? 🥰😍😘😚🤗😻💋💌💘💝💖💗 Coba eksekusi image fhinnn/sisop23 deh, siapa tahu membantu. 🙂

### Catatan

-   Pastikan dalam FUSE tersebut (baik yang normal dari /etc maupun yang telah dimodifikasi) dapat melakukan operasi mkdir, cat dan mv.
-   Pastikan source mount kalian dari .zip yang udah dikasih (inifolderetc/sisop) dan JANGAN PAKAI /etc ASLI KALIAN! kerusakan Linux bukan tanggung jawab kami.

### Permasalahan Poin a - d

-   Tujuan Permasalahan :

    Membuat program bernama `secretadmirer.c` untuk melakukan FUSE mount dengan sourcenya adalah `inifolderetc/sisop` yang diberikan dimana nantinya harus dilakukan encode Base64 pada setiap file yang berada pada directory yang namanya diawali L, U, T, dan H. Selain itu, juga harus dilakukan penggantian nama untuk setiap directory menjadi huruf besar semua, file menjadi huruf kecil semua, sedangkan untuk directory dan file yang namanya kurang dari atau sama dengan 4 huruf harus diconvert menjadi ASCII code masing-masing karakternya. Terakhir, setiap ingin membuka file, harus diminta password terlebih dahulu dan file hanya bisa dibuka saat password sudah benar.

-   Konsep Penyelesaian :

    -   Membuat program bernama secretadmirer.c yang dapat melakukan FUSE mount dengan dirpath mengarah ke `inifolderetc/sisop`
    -   Membuat fungsi untuk melakukan encode isi file ke Base64 dan fungsi untuk mengecek apakah suatu file berada dalam directory yang namanya diawali dengan L, U, T, atau H. Keduanya akan dipanggil dan digunakan pada fungsi readdir untuk FUSE
    -   Membuat fungsi untuk melakukan pergantian nama pada file/directory beserta dengan mengganti pathnya dan juga fungsi untuk melakukan convert terhadap nama file menjadi ASCII code per karakter. Keduanya akan dipanggil dan digunakan pada fungsi readdir untuk FUSE
    -   Membuat fungsi open untuk FUSE dan meminta password setiap kali sebuah file dikenai aksi open

-   Implementasi dan Penjelasan (Kode) :

    1. Import library yang diperlukan dan mendefinisikan versi FUSE serta size buffer untuk encode base64, diikuti dengan inisialisasi variabel path directory source mount dan juga set password

        ```c
        #define FUSE_USE_VERSION 28
        #define BUFFER_SIZE 1024

        #include <fuse.h>
        #include <stdio.h>
        #include <string.h>
        #include <unistd.h>
        #include <fcntl.h>
        #include <dirent.h>
        #include <errno.h>
        #include <sys/time.h>
        #include <stdlib.h>
        #include <ctype.h>
        #include <stdbool.h>
        #include <openssl/bio.h>
        #include <openssl/evp.h>

        static const char *dirpath = "/home/kev/Kuliah/Sisop/Praktikum4/files/inifolderetc/sisop";
        static const char *setPW = "terserah";
        ```

    2. Membuat fungsi base64Encode yang berfungsi untuk melakukan encoding terhadap file yang pathnya dipassing ke dalam fungsi sebagai parameter. Bekerja dengan memanfaatkan library openssl dengan membaca file lama per byte dan melakukan encoding kemudian menuliskannya di temp file. Setelahnya, diikuti dengan pembacaan dan penulisan balik secara per byte dari temp file ke file lama

        ```c
        void base64Encode(const char *filePath) {
            FILE *file, *tempFile;
            BIO *bio, *b64;
            char buffer[BUFFER_SIZE];
            size_t bytesRead;

            file = fopen(filePath, "rb");
            if (file == NULL) {
                printf("Failed to open the file.\n");
                return;
            }

            tempFile = tmpfile();
            if (tempFile == NULL) {
                printf("Failed to create a temporary file.\n");
                fclose(file);
                return;
            }

            b64 = BIO_new(BIO_f_base64());
            bio = BIO_new_fp(tempFile, BIO_NOCLOSE);
            bio = BIO_push(b64, bio);

            while ((bytesRead = fread(buffer, 1, BUFFER_SIZE, file)) > 0) {
                BIO_write(bio, buffer, bytesRead);
            }

            BIO_flush(bio);

            rewind(tempFile);

            fclose(file);
            file = fopen(filePath, "wb");
            if (file == NULL) {
                printf("Failed to open the file for writing.\n");
                fclose(tempFile);
                return;
            }

            while ((bytesRead = fread(buffer, 1, BUFFER_SIZE, tempFile)) > 0) {
                fwrite(buffer, 1, bytesRead, file);
            }

            BIO_free_all(bio);
            fclose(file);
            fclose(tempFile);
            return;
        }
        ```

    3. Membuat fungsi untuk mengecek apakah suatu path berada di dalam suatu directory dengan nama yang diawali L, U, T, atau H. Juga dilakukan checking untuk ASCII karena kode saya melakukan penggantian nama terlebih dahulu sebelum base64 encoding

        ```c
        bool needEncode(const char *string) {
            if (string == NULL) return false;
            return (strstr(string, "/L") != NULL || strstr(string, "/01101100") != NULL || strstr(string, "/U") != NULL || strstr(string, "/01110101") != NULL || strstr(string, "/T") != NULL || strstr(string, "/01110100") != NULL || strstr(string, "/H") != NULL || strstr(string, "/01101000") != NULL);
        }
        ```

    4. Membuat fungsi toUpper dan toLower untuk melakukan konversi terhadap string menjadi huruf besar semua untuk nama directory atau huruf kecil semua untuk nama file

        ```c
        void toUpper(char *str) {
            while (*str) {
                *str = toupper((unsigned char)*str);
                str++;
            }
        }

        void toLower(char *str) {
            while (*str) {
                *str = tolower((unsigned char)*str);
                str++;
            }
        }
        ```

    5. Membuat fungsi untuk melakukan konversi dari nama file yang dipassing menjadi ASCII code per karakternya dan kemudian mengembalikan hasilnya

        ```c
        char* asciiConv(const char* name) {
            int length = strlen(name);
            char* result = (char*)malloc((9 * length + 1) * sizeof(char));
            if (result == NULL) {
                fprintf(stderr, "Memory allocation failed\n");
                return NULL;
            }

            int index = 0;
            for (int i = 0; i < length; i++) {
                unsigned char c = name[i];
                for (int j = 7; j >= 0; j--) {
                    int bit = (c >> j) & 1;
                    index += sprintf(result + index, "%d", bit);
                }
                result[index++] = ' ';
            }
            result[index - 1] = '\0';

            return result;
        }
        ```

    6. Membuat fungsi changeName yang mengimplementasikan fungsi rename() untuk mengganti nama dari sebuah file/directory tetapi dengan tambahan juga mengganti pathnya untuk menyesuaikan dengan nama barunya

        ```c
        void changeName(char *path, const char *newName) {
            char newPath[1000];
            strncpy(newPath, path, sizeof(newPath));
            newPath[sizeof(newPath) - 1] = '\0';

            char *lastSlash = strrchr(newPath, '/');
            if (lastSlash != NULL) {
                strcpy(lastSlash + 1, newName);
                rename(path, newPath);

                strncpy(path, newPath, strlen(newPath) + 1);
            }
        }
        ```

    7. Membuat fungsi xmp_getattr sebagai fungsi getattr dari struct FUSE

        ```c
        static int xmp_getattr(const char *path, struct stat *stbuf)
        {
            int res;
            char fpath[1000];

            sprintf(fpath,"%s%s",dirpath,path);

            res = lstat(fpath, stbuf);

            if (res == -1) return -errno;

            return 0;
        }
        ```

    8. Membuat fungsi xmp_readdir sebagai fungsi readdir dari struct FUSE yang akan digunakan untuk melakukan penggantian nama dan juga encoding base64 secara rekursif terhadap keseluruhan directory FUSE

        ```c
        static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
        {
            char fpath[1000];

            if (strcmp(path, "/") == 0)
            {
                path = dirpath;
                sprintf(fpath, "%s", path);
            }
            else sprintf(fpath, "%s%s", dirpath, path);

            int res = 0;

            DIR *dp;
            struct dirent *de;
            (void)offset;
            (void)fi;

            dp = opendir(fpath);

            if (dp == NULL) return -errno;

            while ((de = readdir(dp)) != NULL) {
                if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
                    continue;

                struct stat st;
                char cpath[2000];

                memset(&st, 0, sizeof(st));

                st.st_ino = de->d_ino;
                st.st_mode = de->d_type << 12;

                sprintf(cpath, "%s/%s", fpath, de->d_name);

                res = (filler(buf, de->d_name, &st, 0));
                if (res!=0) break;

                if (S_ISDIR(st.st_mode)) {
                    char dName[1000];
                    strcpy(dName, de->d_name);

                    if (strlen(dName) <= 4) {
                        strcpy(dName, asciiConv(dName));
                    } else {
                        toUpper(dName);
                    }
                    changeName(cpath, dName);

                    xmp_readdir(cpath, buf, filler, offset, fi);
                } else {
                    char fName[1000];
                    strcpy(fName, de->d_name);

                    if (strlen(fName) <= 4) {
                        strcpy(fName, asciiConv(fName));
                    } else {
                        toLower(fName);
                    }
                    changeName(cpath, fName);

                    if (needEncode(cpath)) base64Encode(cpath);
                }
            }

            closedir(dp);
            return 0;
        }
        ```

    9. Membuat fungsi xmp_read sebagai fungsi read dari struct FUSE

        ```c
        static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
        {
            char fpath[1000];
            if(strcmp(path,"/") == 0)
            {
                path=dirpath;

                sprintf(fpath,"%s",path);
            }
            else sprintf(fpath, "%s%s",dirpath,path);

            int res = 0;
            int fd = 0 ;

            (void) fi;

            fd = open(fpath, O_RDONLY);

            if (fd == -1) return -errno;

            res = pread(fd, buf, size, offset);

            if (res == -1) res = -errno;

            close(fd);

            return res;
        }
        ```

    10. Membuat fungsi xmp_open sebagai fungsi open dari struct FUSE yang juga berfungsi untuk meminta password dari pengguna setiap kali sebuah file dibuka dan baru mengizinkan file untuk dibuka setelah password yang benar dimasukkan

        ```c
        static int xmp_open(const char *path, struct fuse_file_info *fi)
        {
            (void)fi;

            if (strcmp(path, "/") == 0) {
                path = dirpath;
            } else {
                char fpath[1000];
                sprintf(fpath, "%s%s", dirpath, path);
                if (access(fpath, F_OK) == 0) {
                    char inPW[100];
                    printf("Enter your Password: ");
                    fflush(stdout);
                    fgets(inPW, sizeof(inPW), stdin);

                    int len = strlen(inPW);
                    if (len > 0) inPW[len - 1] = '\0';

                    while (strcmp(inPW, setPW) != 0) {
                        printf("The password you just entered is incorrect! Please try again.\n\nEnter your Password: ");
                        fflush(stdout);
                        fgets(inPW, sizeof(inPW), stdin);

                        len = strlen(inPW);
                        if (len > 0) inPW[len - 1] = '\0';
                    }
                }
            }

            return 0;
        }
        ```

    11. Membuat fungsi xmp_rename dan xmp_mkdir sebagai fungsi rename dan mkdir dari struct FUSE agar kedua aksi tersebut diperbolehkan sehingga perintah `mkdir` dan `mv` dapat dilakukan sesuai dengan perintah permasalahan

        ```c
        int xmp_rename(const char *old, const char *new)
        {
            char oldPath[1000], newPath[1000];
            sprintf(oldPath, "%s%s", dirpath, old);
            sprintf(newPath, "%s%s", dirpath, new);

            int res = rename(oldPath, newPath);
            if (res == -1)
                return -errno;

            return 0;
        }

        int xmp_mkdir(const char *path, mode_t mode)
        {
            char dirPath[1000];
            sprintf(dirPath, "%s%s", dirpath, path);

            int res = mkdir(dirPath, mode);
            if (res == -1)
                return -errno;

            return 0;
        }
        ```

    12. Membuat struct FUSE utama dan memanggil fuse_main pada main setelah melakukan umask untuk mengatur privilege/hak akses

        ```c
        static struct fuse_operations xmp_oper = {
            .getattr = xmp_getattr,
            .readdir = xmp_readdir,
            .read = xmp_read,
            .open = xmp_open,
            .rename = xmp_rename,
            .mkdir = xmp_mkdir,
        };

        int main(int  argc, char *argv[])
        {
            umask(0);
            return fuse_main(argc, argv, &xmp_oper, NULL);
        }
        ```

-   Dokumentasi Hasil :

    -   Dokumentasi penggantian nama dari file dan directory<br>
        <img src="https://cdn.discordapp.com/attachments/995337235211763722/1114399684702777354/image.png">

    -   Dokumentasi request password ketika membuka file<br>
        <img src="https://cdn.discordapp.com/attachments/995337235211763722/1114400830142025798/image.png">

    -   Dokumentasi berhasil membuka file setelah password benar dan menunjukkan hasil encode base64<br>
        <img src="https://cdn.discordapp.com/attachments/995337235211763722/1114400885313896538/image.png">

    -   Dokumentasi melakukan mkdir<br>
        <img src="https://cdn.discordapp.com/attachments/995337235211763722/1114402656878858250/image.png">

    -   Dokumentasi melakukan mv<br>
        <img src="https://cdn.discordapp.com/attachments/995337235211763722/1114402782645067776/image.png">

### Permasalahan Poin e

-   Tujuan Permasalahan :

    Melakukan mounting source directory FUSE tersebut ke dalam container bernama 'Dhafin' dan melakukan mounting dari directory etc yang tidak dimodifikasi ke dalam container bernama 'Normal'

-   Konsep Penyelesaian :

    -   Membuat `docker-compose.yml` untuk kedua task di atas
    -   Melakukan `docker compose up -d`

-   Implementasi dan Penjelasan (`docker-compose.yml`) :

    Membuat file yml yang berisikan config untuk docker compose diawali dengan versinya, kemudian membuat 2 container dalam services dimana keduanya menggunakan image ubuntu:bionic. Nama container pertama adalah 'Dhafin' dan yang kedua adalah 'Normal'. Kemudian untuk command yang digunakan juga dituliskan dimana yang pertama menggunakan command `tail -f /dev/null` sesuai dengan modul dan yang kedua menggunakan command `sleep infinity` untuk membuat container terus berjalan hingga dilakukan `docker compose down`. Terakhir, untuk volume bind mountnya, container pertama menggunakan source yakni folder `/inifolderetc/sisop` yang saya letakkan dalam folder Sistem Operasi saya dan diarahkan ke target directory bernama `/fuse_dhafin`. Sementara itu, untuk yang kedua menggunakan source `etc` yang merupakan tempat saya menyimpan directory source yang tidak dimodifikasi dan diarahkan ke target directory bernama `/fuse_normal`.

    ```yml
    version: "3"

    services:
        dhafin:
            image: ubuntu:bionic
            container_name: Dhafin
            command: tail -f /dev/null
            volumes:
                - /home/kev/Kuliah/Sisop/Praktikum4/files/inifolderetc/sisop:/fuse_dhafin

        normal:
            image: ubuntu:bionic
            container_name: Normal
            volumes:
                - /home/kev/Kuliah/Sisop/Praktikum4/soal3/etc:/fuse_normal
            command: sleep infinity
    ```

-   Dokumentasi Hasil :

    -   Dokumentasi melakukan `docker compose up` dan mengecek hasilnya dengan ps<br>
        <img src="https://cdn.discordapp.com/attachments/995337235211763722/1114405550730518589/image.png">

    -   Dokumentasi melakukan `docker exec -it Dhafin /bin/bash` dan mengecek isi dari directory `/fuse_dhafin`<br>
        <img src="https://cdn.discordapp.com/attachments/995337235211763722/1114405977115086918/image.png">

    -   Dokumentasi melakukan `docker exec -it Normal /bin/bash` dan mengecek isi dari directory `/fuse_normal`<br>
        <img src="https://cdn.discordapp.com/attachments/995337235211763722/1114407314951262258/image.png">

### Permasalahan Poin f

-   Tujuan Permasalahan :

    Mencari tahu nama yang dikagumi Dhafin dengan mengeksekusi image `fhinnn/sisop23`

-   Konsep Penyelesaian :

    -   Melakukan `docker run` terhadap image `fhinnn/sisop23`
    -   Mempelajari tentang directory `etc` di Linux
    -   Mencari dan mengamati perbedaan antara directory etc yang asli di Linux dan directory `inifolderetc/sisop` yang diberikan

-   Penyelesaian :

    1. Melakukan `docker run` terhadap image `fhinnn/sisop23`<br>
       <img src="https://cdn.discordapp.com/attachments/995337235211763722/1114408173185544222/image.png">

    2. Mempelajari tentang directory `etc` di Linux, kemudian mencari perbedaan antara directory etc yang asli di Linux dan directory `inifolderetc/sisop` yang diberikan. Ditemukan sebuah kejanggalan yakni satu directory dengan nama yang aneh (bahasa indonesia, terlihat seperti buatan sendiri)<br>
       <img src="https://cdn.discordapp.com/attachments/995337235211763722/1114409011014537326/image.png">

    3. Masuk ke directory tersebut dan melihat isinya<br>
       <img src="https://cdn.discordapp.com/attachments/995337235211763722/1114409036381687938/image.png">

    4. Melihat isi dari `New Text Document.txt` dan melakukan decode terhadap ASCII code yang ada disana<br>
       <img src="https://cdn.discordapp.com/attachments/995337235211763722/1114409063216844841/image.png">

    5. Didapatkan hasil berupa string yaitu <b>"h4nif4h_ama12i"</b>

### Kendala yang sempat dihadapi

-   Ketidakpahaman secara penuh terhadap cara kerja FUSE yang membuat waktu pengerjaan harus diawali dengan belajar terlebih dahulu sehingga memakan waktu lama
-   Agak lupa mengenai cara membuat docker container langsung dengan docker-compose.yml
